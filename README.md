# Wall wart ±12V power supply

This is a power supply based on the [MFOS wall wart supply](http://musicfromouterspace.com/analogsynth_new/WALLWARTSUPPLY/WALLWARTSUPPLY.php). Differences are:

* Added polyfuses
* Added LEDs
* Input is either barrel jack or pluggable terminal block, with another pluggable terminal block for daisy chaining
* Output is a (non pluggable) terminal block
* Regulators are vertical, with space for Tayda A-5093 heat sinks
* 1N4004 on positive regulator output changed to 1N5817*
* Board is shorter and wider (100 x 61 mm) to keep fabbing price low

\* My reading suggests the diodes between the outputs and ground are *not* needed for protection in this case. But a Schottky between the positive rail and ground helps guard against a startup failure mode I was seeing where the negative supply comes on first and the positive regulator can't start with its output pulled negative. The opposite situation doesn't arise due to differences between positive and negative regulators; I left the 1N4004 on the negative rail since it seems to do no harm and maybe it does do some good. For details see [Texas Instruments "AN-182 Improving Power Supply Reliability with IC Power Regulators"](https://www.ti.com/lit/pdf/snva517).

Note that unlike usual Eurorack supplies, this one provides no +5 V rail. (Of course there are almost no Eurorack modules that use the +5 V rail these days, and no Kosmo modules do.)

I've built a version of this board and it works. I found a few minor issues in the process, the worst of which was an incorrect footprint for the fuses with too small holes for the leads — no other problems affecting functionality. I've since updated the layout to fix the issues I've found. I haven't built that version yet, but it should be fine.

![](Images/ww_supplies.jpg)

## Documentation

* [Schematic](Docs/mfosish_ww_supply.pdf)
* [PCB layout](Docs/mfosish_ww_supply_layout.pdf)
* [BOM](Docs/mfosish_ww_supply_bom.md)

## Git repository

* [https://gitlab.com/rsholmes/ww_supply](https://gitlab.com/rsholmes/ww_supply)


